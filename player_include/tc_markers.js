/*
 * Video.js TcMarkers
 * TC Markers
 * Requires jQuery
 *
 * Copyright (c) 2016 Pavel Yanovsky
 */
 
 (function(window, videojs) {
  'use strict';

  window['videojs_tc_markers'] = { version: "0.0.1" };
  
  var tc_markers = function(options) {
    
	//default settings of tc markers (here you can change style of tc_marker on timeline)
	
	var tc_defaultSetting = {
        markerStyle: {
           'width':'2px',
           'background-color': 'green',
        },
        markerTip: {
           display: true,
           text: function(marker) {
              return "Break: "+ marker.text;
           },
           time: function(marker) {
              return marker.time;
           },
        },
        breakOverlay:{
           display: false,
           displayTime: 3,
           text: function(marker) {
              return "Break overlay: " + marker.overlayText;
           },
           style: {
              'width':'100%',
              'height': '20%',
              'background-color': 'rgba(0,0,0,0.7)',
              'color': 'white',
              'font-size': '17px'
           }
        },
        
		//action when marker is clicked
		onMarkerClick: function(marker) {

        },
		
        onMarkerReached: function(marker) {},
        markers: []

     };
	//////////////////////////////////////////////////////////////////////////////
	
	 // create a non-colliding random number
     function tc_generateUUID() {
        var d = new Date().getTime();
        var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
           var r = (d + Math.random()*16)%16 | 0;
           d = Math.floor(d/16);
           return (c=='x' ? r : (r&0x3|0x8)).toString(16);
        });
        return uuid;
     };
	 
	//////////////////////////////////////////////////////////////////////////////
	
	//settings of tc_markers (here we use default settings)
	
	var setting      = $.extend(true, {}, tc_defaultSetting, options),
            markersMap   = {},
            markersList  = [], // list of markers sorted by time
            videoWrapper = $(this.el()),
            currentMarkerIndex  = -1, 
            player       = this,
            markerTip    = null,
            breakOverlay = null,
            overlayIndex = -1;
			
	///////////////////////////////////////////////////////////////////////////////
	
	function tc_sortMarkersList() {
           // sort the list by time in asc order
           markersList.sort(function(a, b){
              return setting.markerTip.time(a) - setting.markerTip.time(b);
           });
        }
		
	////////////////////////////////////////////////////////////////////////////////
	//main function for adding markers (using in player.tc_markers.add)
	
	 function tc_addMarkers(newMarkers) {
           // create the markers
           $.each(newMarkers, function(index, marker) {
              marker.key = tc_generateUUID();

              videoWrapper.find('.vjs-progress-control').append(
                 tc_createMarkerDiv(marker));
             
			 videoWrapper.find('.vjs-progress-control').append(
                 tc_createSectionFillDiv(marker));
			
              // store marker in an internal hash map
              markersMap[marker.key] = marker;
              markersList.push(marker);          
           });

           tc_sortMarkersList();
        }
		
	////////////////////////////////////////////////////////////////////////////////////
	
	//get position of marker in percents of player's width
	
	function tc_getPosition(marker){
           return ((setting.markerTip.time(marker) / player.duration()) * 100);
        }
	 
	/////////////////////////////////////////////////////////////////////////////////////
	
	//creating div on timeline for marker
	
	function tc_createMarkerDiv(marker, duration) {

           var tc_flag_marker = true;

           for (var i = 0; i< markersList.length; i++) {
		   
              var Xmarker = markersList[i];
              var XmarkerDiv = videoWrapper.find(".vjs-tc-marker[data-marker-key='" + Xmarker.key +"']"); 

              if (Xmarker.type == 'tc_in') {

                if (Xmarker.time>=marker.time) {

                  tc_flag_marker = false; // flag for changing position of marker or adding marker

                };

              };

           };


          if (tc_flag_marker) {
		   var markerDiv = $(document.createElement('div'));
           markerDiv.css(setting.markerStyle)
              .css({"margin-left" : -parseFloat(markerDiv.css("width"))/2 + 'px', 
                 "left" : tc_getPosition(marker) + '%', "z-index" : 1})
              .attr("data-marker-key", marker.key)
              .attr("data-marker-time", setting.markerTip.time(marker));
			markerDiv.addClass('vjs-tc-marker');  
			
           // add user-defined class to marker
           if (marker.class) {
              markerDiv.addClass(marker.class);
           }

           // bind click event to seek to marker time
           markerDiv.on('click', function(e) {

              var preventDefault = false;
              if (typeof setting.onMarkerClick === "function") {
                 // if return false, prevent default behavior
                 preventDefault = setting.onMarkerClick(marker) == false;
              }

              if (!preventDefault) {
                 var key = $(this).data('marker-key');
                 player.currentTime(setting.markerTip.time(markersMap[key]));
              }


           });

           return markerDiv;
         };
    }
		
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		//function for creating filled span between two points (markers or one of the ends of timeline)
		
		function tc_createSectionFillDiv(marker) {

           if (marker.type =='tc_in') { //markers can have only two types: tc_in and tc_out

            var tc_section_width = 100-tc_getPosition(marker); //width of section from the tc_in marker to the end of timeline in percent
            
            if (!player.tc_out) {

              player.tc_out = MillToTimecode(player.duration(), 'PAL'); //using function from timecode plugin (MillToTimecode function)

            }
			

            for (var i = 0; i< markersList.length; i++) {
              var Xmarker = markersList[i];
              var XmarkerDiv = videoWrapper.find(".vjs-tc-marker[data-marker-key='" + Xmarker.key +"']");
			  

              if (Xmarker.type == 'tc_out') { //markers can have only two types: tc_in and tc_out

                if (Xmarker.time<=marker.time) {

                  //XmarkerDiv.remove();
				  player.tc_markers.remove([i]);
                  videoWrapper.find(".vjs-section").remove();
                  player.tc_out = MillToTimecode(player.duration(), 'PAL');

                } else {

                  tc_section_width = Math.round((XmarkerDiv.width() + XmarkerDiv.position().left)/XmarkerDiv.parent().width()*100 - tc_getPosition(marker)); 

                };

              };

           };
		   
            var sectionDiv;


            if (videoWrapper.find(".vjs-section").length == 0) {

              sectionDiv = $("<div class='vjs-section' style='z-index:0; position:absolute'></div>"); 

            } else {

              sectionDiv = videoWrapper.find(".vjs-section");

            };

            sectionDiv
              .css({"left" : tc_getPosition(marker) + '%'})
              .attr("data-tc-marker-key", marker.key)
              .css("width",  tc_section_width + '%') 
              .css("opacity", '0.2') 
              .css("background-color", 'green');

           } else { //case when marker.type =='tc_out'

             var sectionDiv;
             var SD_WidthPercentage;
             var tc_section_width = tc_getPosition(marker);
             var tc_flag = true; //for checking if we need to change (add) marker on timeline 

             for (var i = 0; i< markersList.length; i++) {
              var Xmarker = markersList[i];
              var XmarkerDiv = videoWrapper.find(".vjs-tc-marker[data-marker-key='" + Xmarker.key +"']"); 

              if (Xmarker.type == 'tc_in') {

                if (Xmarker.time>=marker.time) {

                  tc_section_width = 0;
                  tc_flag = false;

                } else {

                  tc_section_width = tc_getPosition(marker) - (XmarkerDiv.position().left/XmarkerDiv.parent().width()*100); 
                  tc_flag = true; 
                };

              };

           };
           
            if (tc_flag) {

              if (videoWrapper.find(".vjs-section").length == 0) { 

                sectionDiv = $("<div class='vjs-section' style='z-index:0; position:absolute'></div>");

                sectionDiv
                .css({ 
                 "left" : '0%'})  
                .css("opacity", '0.4')  
                .css("background-color", 'green');

                player.tc_in = MillToTimecode(0, 'PAL'); // using function from timecode plugin

             } else {

                sectionDiv = videoWrapper.find(".vjs-section");
                SD_WidthPercentage = sectionDiv.position().left/sectionDiv.parent().width()*100;


             }
             
             sectionDiv 
              .css("width", (tc_section_width)+'%');

            }; 


           }

           return sectionDiv;
        } 
		
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	 function tc_updateMarkers() {
           // update UI for markers whose time changed

           for (var i = 0; i< markersList.length; i++) {
              var marker = markersList[i];
              var markerDiv = videoWrapper.find(".vjs-tc-marker[data-marker-key='" + marker.key +"']"); 
              var markerTime = setting.markerTip.time(marker);

              if (markerDiv.data('marker-time') != markerTime) {
                 markerDiv.css({"left": tc_getPosition(marker) + '%'})
                    .attr("data-marker-time", markerTime);
              }
           }
           tc_sortMarkersList(); 
        }
		
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	 function tc_removeMarkers(indexArray) {
           // reset overlay
           if (breakOverlay){
               overlayIndex = -1;
               breakOverlay.css("visibility", "hidden");
           }
           currentMarkerIndex = -1;

           for (var i = 0; i < indexArray.length; i++) {
              var index = indexArray[i];
              var marker = markersList[index];
              if (marker) {
                 // delete from memory
                 delete markersMap[marker.key];
                 markersList[index] = null;

                 // delete from dom
                 videoWrapper.find(".vjs-tc-marker[data-marker-key='" + marker.key +"']").remove();
              }
           }

           // clean up array
           for (var i = markersList.length - 1; i >=0; i--) {
              if (markersList[i] === null) {
                 markersList.splice(i, 1);
              }
           }

           // sort again
           tc_sortMarkersList();
        }
		
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		// show or hide break overlays
        function tc_updateBreakOverlay() {
           if(!setting.breakOverlay.display || currentMarkerIndex < 0){
              return;
           }

           var currentTime = player.currentTime();
           var marker = markersList[currentMarkerIndex];
           var markerTime = setting.markerTip.time(marker);

           if (currentTime >= markerTime && 
              currentTime <= (markerTime + setting.breakOverlay.displayTime)) {
              if (overlayIndex != currentMarkerIndex){
                 overlayIndex = currentMarkerIndex;
                 breakOverlay.find('.vjs-tc-break-overlay-text').text(setting.breakOverlay.text(marker));
              }

              breakOverlay.css('visibility', "visible");

           } else {
              overlayIndex = -1;
              breakOverlay.css("visibility", "hidden");
           }
        }
		
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		// problem when the next marker is within the overlay display time from the previous marker
        function tc_initializeOverlay() {
           breakOverlay = $("<div class='vjs-tc-break-overlay'><div class='vjs-tc-break-overlay-text'></div></div>")
              .css(setting.breakOverlay.style);
           videoWrapper.append(breakOverlay);
           overlayIndex = -1;
        }
		
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		function tc_onTimeUpdate() {
		
           tc_updateBreakOverlay();
		   
        }
		
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		 // setup the whole thing
        function tc_initialize() {

           // remove existing markers if already initialized
           player.tc_markers.removeAll();

           if (setting.breakOverlay.display) {
              tc_initializeOverlay();
           }
           tc_onTimeUpdate();
           player.on("timeupdate", tc_onTimeUpdate);
        }
		
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		// setup the plugin after we loaded video's meta data
        player.on("loadedmetadata", function() {
           tc_initialize();
        });
		
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		// exposed plugin API (little API for using tc_markers
		
        player.tc_markers = {
           getMarkers: function() {
             return markersList;
           },
           next : function() {
              // go to the next marker from current timestamp
              var currentTime = player.currentTime();
              for (var i = 0; i < markersList.length; i++) {
                 var markerTime = setting.markerTip.time(markersList[i]);
                 if (markerTime > currentTime) {
                    player.currentTime(markerTime);
                    break;
                 }
              }
           },
           prev : function() {
              // go to previous marker
              var currentTime = player.currentTime();
              for (var i = markersList.length - 1; i >=0 ; i--) {
                 var markerTime = setting.markerTip.time(markersList[i]);
                 // add a threshold
                 if (markerTime + 0.5 < currentTime) {
                    player.currentTime(markerTime);
                    break;
                 }
              }
           },
           add : function(newMarkers) {
              // add new markers given an array of index
              tc_addMarkers(newMarkers);
           },
           remove: function(indexArray) {
              // remove markers given an array of index
              tc_removeMarkers(indexArray);
           },
           removeAll: function(){
              var indexArray = [];
              for (var i = 0; i < markersList.length; i++) {
                 indexArray.push(i);
              }
              tc_removeMarkers(indexArray);
           },
           updateTime: function(){
              // notify the plugin to update the UI for changes in marker times 
              tc_updateMarkers();
           },
           reset: function(newMarkers){
              // remove all the existing markers and add new ones
              player.markers.removeAll();
              tc_addMarkers(newMarkers);
           },
           destroy: function(){
              // unregister the plugins and clean up even handlers
              player.tc_markers.removeAll();
              breakOverlay.remove();
              markerTip.remove();
              player.off("timeupdate", tc_updateBreakOverlay);
              delete player.tc_markers;
           },
        };
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//adding tc_in button on control panel in player
	
	var VjsButton = videojs.getComponent('Button');
	
	
	player.Tc_in_btn = videojs.extend(VjsButton, {
     contructor: function(){

          VjsButton.call(this, player, options);
          this.on('click', this.onClick());

     },

  });

   //action after clicking tc_in button
  player.Tc_in_btn.prototype.handleClick = function() {
	
    player.pause();

    var tc_gMarkers = player.tc_markers.getMarkers();

    
	for (var i = 0; i< tc_gMarkers.length; i++) {
	
        if (tc_gMarkers[i].type == 'tc_in') {

			player.tc_markers.remove([i]);

        } else if (tc_gMarkers[i].time<=player.currentTime()) {
			
			player.tc_markers.remove([i]);
		
		};
		
		

    };
	  

	//adding tc_in marker
    player.tc_markers.add([{
                time: player.currentTime(),
                type: 'tc_in'
             }]);
			 
	player.tc_in = MillToTimecode(player.currentTime(), 'PAL');  //using function from timecode plugin
    //example of using information in player.tc_in (you should have div with id "test" in html or change id in code below
	
	if ((player.tc_out))  {

      $('#tc_in_out_info').html("<div>player.tc_in = "+player.tc_in+"</div><div>player.tc_out = "+player.tc_out+"</div>");

    } else {

      $('#tc_in_out_info').html("<div>player.tc_in = "+player.tc_in+"</div>");

    };  
  };

  var tc_in_btn = player.controlBar.addChild(new player.Tc_in_btn, {});

  tc_in_btn.addClass("vjs-tc-in-button");  


  //adding tc_out button on control panel in player
  
  player.Tc_out_btn = videojs.extend(VjsButton, {
     contructor: function(){

          VjsButton.call(this, player, options);
          this.on('click', this.onClick());

     },

  });

  player.Tc_out_btn.prototype.handleClick = function() {
  
    player.pause();
    var tc_flag_add = true;

    var tc_gMarkers = player.tc_markers.getMarkers();

    for (var i = 0; i< tc_gMarkers.length; i++) {
	  
        if ((tc_gMarkers[i].type == 'tc_in') && (tc_gMarkers[i].time >=  player.currentTime()))      {

          tc_flag_add = false;

        };

        if ((tc_gMarkers[i].type == 'tc_out') && (tc_flag_add)) { 

          player.tc_markers.remove([i]);

        }


      };

    if (tc_flag_add) {

      player.tc_markers.add([{
                  time: player.currentTime(),
                  type: 'tc_out'
               }]);

      player.tc_out = MillToTimecode(player.currentTime(), 'PAL');

    //example of using information in player.tc_out (you should have div with id "test" in html or change id in code below
	
	if (player.tc_in)  {

      $('#tc_in_out_info').html("<div>player.tc_in = "+player.tc_in+"</div><div>player.tc_out = "+player.tc_out+"</div>");

    } else {

      $('#tc_in_out_info').html("<div>player.tc_out = "+player.tc_out+"</div>");

    };   

    };

  }

  var tc_out_btn = player.controlBar.addChild(new player.Tc_out_btn, {});

  tc_out_btn.addClass("vjs-tc-out-button");    

	///////////////////////////////////////////////////////////////////////////////////////////////////////////
	//adding tc_in and tc_out markers on timeline when pressed key "i" or "o"
	
	$(document).keyup(function(e) {
           
			
			
            if (e.keyCode == 73) {

              $('.vjs-tc-in-button').click();

             } else if (e.keyCode == 79) {

               $('.vjs-tc-out-button').click();

             };

	});
	
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
    return this;
  };

  videojs.plugin('tc_markers', tc_markers);

})(window, window.videojs);

//Converts time in seconds to a broadcast timecode
//timeFormat: 'PAL', 'PALp', 'NTSC', 'STANDARD'
function MillToTimecode(seconds, TimeFormat) {

    //alert(milliseconds);

    var h = Math.floor(seconds / 3600);

    seconds = seconds - h * 3600;

    var m = Math.floor(seconds / 60);

    seconds = seconds - m * 60;

    var s = Math.floor(seconds);

    seconds = seconds - s;

    if (TimeFormat == 'PAL') {
        var f = Math.floor((seconds * 1000) / 40);
    }
    else if (TimeFormat == 'NTSC') {
        var f = Math.floor((seconds * 1000) / (100 / 3));
    }
    else if (TimeFormat == 'PALp') {
        var f = Math.floor((seconds * 1000) / 20);
    }
    else if (TimeFormat == 'STANDARD') {
        var f = Math.floor(seconds * 1000);
    }

    // Check if we need to show hours
    h = (h < 10) ? ("0" + h) + ":" : h + ":";

    // If hours are showing, we may need to add a leading zero.
    // Always show at least one digit of minutes.
    m = (((h) && m < 10) ? "0" + m : m) + ":";

    // Check if leading zero is need for seconds
    s = ((s < 10) ? "0" + s : s) + ":";

    f = (f < 10) ? "0" + f : f;

    if (TimeFormat == 'STANDARD')
        f = (f < 100) ? "0" + f : f;

    return h + m + s + f;
}
