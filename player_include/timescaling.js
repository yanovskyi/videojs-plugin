/*
 * Video.js TimeScaling
 * Time Scaling on Timeline
 * Requires jQuery
 *
 * Copyright (c) 2016 Pavel Yanovsky
 */
 
 (function(window, videojs) {
  'use strict';

  window['videojs_timescaling'] = { version: "0.0.1" };
  var timescaling = function() {
    var player = this;
	var flag_as = false; // flag for auto scrabbing
	var flag_sbs = false; //flag for scaling mode
	var start_y = 310;
	var max_height = 5; //max height of timeline when we are changing height
	var min_height = 0.9; //min height of timeline when we are changing height
	
	var dHo = 0.9; //minimal difference between old and new positions of mouse
	var Zo = 0; //start point of new timeline's span
	var mt = 0; //new time
	var t = player.currentTime(); //time
	var D = player.duration(); //duration
	var Tp = 0; //position on timeline in percent of width
	
	var mD, dH, pos;
	
	//shadow for all player when scaling mode is activated
	
	var shadow_div = document.createElement("div");
	var playerShadow = player.addChild("button", {
         "el": shadow_div
        });
	playerShadow.el().style.backgroundColor = 'black'; 
	playerShadow.el().style.opacity = '0';
	
	
	///////////////////////////////////////////////////////////////////////////////////////
	
	//function for action after clicking on timeline
	
	function seekBarClicked (e) {

        videojs.getComponent("SeekBar").prototype.getPercent = function () {

            var percent = player.currentTime() / player.duration();
            return percent >= 1 ? 1 : percent;

          };

          player.controlBar.progressControl.seekBar.update(); 

          player.currentTime(player.controlBar.progressControl.seekBar.calculateDistance(e)*player.duration());

        flag_sbs = true; //flag for detecting scaling mode
        start_y = e.clientY;

          playerShadow.el().style.display = 'inline-block';
          playerShadow.el().style.opacity = '0.4';
          playerShadow.el().style.zIndex= '1';
          playerShadow.el().style.position = 'absolute'; 
          playerShadow.el().style.top = "0"; 
          playerShadow.el().style.width = player.el().getBoundingClientRect().width+"px"; 
          playerShadow.el().style.height = player.el().getBoundingClientRect().height+"px"; 

          $(".vjs-control-bar").css('z-index','2'); 

		  $('head').append("<style>.vjs-play-progress:before {display: none; }</style>");
		  $('head').append("<style>.video-js .vjs-progress-control:hover .vjs-play-progress:after {display: none; }</style>");
		  $('head').append("<style>.video-js .vjs-progress-control:hover .vjs-mouse-display:after {display: none; }</style>");
		  
		  
	};
	
	////////////////////////////////////////////////////////////////////////////////////////////
	
	 $('.vjs-progress-control').on("mousedown", function(e) {
	
		seekBarClicked(e);

	});
  
	/////////////////////////////////////////////////////////////////////////////////////////////
	
	player.on("mouseup",  function (e) {

        if (flag_sbs) {
			
			setTimeout(function () {
				
				if (!flag_sbs) {
				
					$('head').append("<style>.vjs-play-progress:before {display: inline-block; }</style>");
					$('head').append("<style>.video-js .vjs-progress-control:hover .vjs-play-progress:after {display: inline-block; }</style>");
					$('head').append("<style>.video-js .vjs-progress-control:hover .vjs-mouse-display:after {display: inline-block; }</style>");
				
				}
			
			}, 300);
		
		}
		
		flag_sbs = false; //flag for detecting scaling mode

        playerShadow.el().style.opacity = '0';
        playerShadow.el().style.display = 'none';
        $('.vjs-ruler').remove();

		

        $(".vjs-progress-control").hover(function() {

          $(".vjs-progress-control").css("font-size", "0.9em"); 

        }, function() {

          $(".vjs-progress-control").css("font-size", "0.5em"); 

        }); 

	});
	
	///////////////////////////////////////////////////////////////////////////////////////////////
	
	player.on("mousedown",  function (e) {

		videojs.getComponent("SeekBar").prototype.getPercent = function () {

            var percent = player.currentTime() / player.duration();
            return percent >= 1 ? 1 : percent;

          };

		player.controlBar.progressControl.seekBar.update(); 

		if(flag_sbs) {

			t = player.currentTime();
			D = player.duration();
			Tp = player.controlBar.progressControl.seekBar.calculateDistance(e); //position on timeline in percent of width
		}


	});
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	
	player.on("mousemove",  function (e) {

        if(flag_sbs) { //check for active scaling mode
		  
		  pos = (start_y - e.clientY)/10;  

          if ((pos>min_height) && (pos < max_height )) {

            $(".vjs-progress-control").css("font-size", pos+"em"); //change height of timeline

          }

          if (pos<=min_height) {

            dH = min_height;

          } else if (pos>=max_height) {

            dH = max_height;

          } else {

            dH = pos;

          };

          mD = 102/(0.1+dH); //new duration after scaling

          if (mD>D) {

            mD=D;

          }

          if (dHo != dH) {

             Zo = t-(Tp*mD);

             dHo = dH;

            if (Zo <0) {

              Zo=0;

            };

          };

          mt = (Zo+Tp*mD); //calculating new time after scaling 

          player.currentTime(Math.round(mt*10000)/10000);

          $('.vjs-ruler').remove(); 

          if (dH > 0.9)  {

             $('.vjs-marker').css('display', 'none');
             $('.vjs-tc-marker').css('display', 'none');
             $('.vjs-section').css('display', 'none');

            rulers(); //function for building rulers on timeline

          videojs.getComponent("SeekBar").prototype.getPercent = function () {

            var percent = (Math.round(Tp*100)/100);
            return percent >= 1 ? 1 : percent; 

          };

          player.controlBar.progressControl.seekBar.update(); 

	         
	
	
	
  flag_as = true;

  auto_scrubbing();

          }; 

          t=player.currentTime();

           Tp = player.controlBar.progressControl.seekBar.calculateDistance(e);


        }

  });
  
  
	//function for building rulers on timeline
	function rulers () {

          var ruler_begin = $("<div style='z-index:5; background-color: black; position:absolute; color:white; font-size:12px;'></div>");
          var ruler_end = $("<div style='z-index:5; background-color: black; position:absolute; color:white; font-size:12px;'></div>");
          var ruler_middle = $("<div style='z-index:5; background-color: black; position:absolute; color:white; font-size:12px;'></div>");

          $('.video-js').append(ruler_begin); 
          $('.video-js').append(ruler_middle);
          $('.video-js').append(ruler_end);  

          ruler_begin.css("left", "0");
          ruler_begin.css("margin", "3px"); 
          ruler_begin.css("bottom", ($('.vjs-progress-control').height()));
          ruler_begin.text(MillToTimecode(Zo, 'PAL'));
          ruler_begin.addClass("vjs-ruler"); 

          ruler_middle.css("left", "45%");
          ruler_middle.css("margin", "3px"); 
          ruler_middle.css("bottom", ($('.vjs-progress-control').css('height')));
          ruler_middle.text(MillToTimecode(player.currentTime(), 'PAL'));
          ruler_middle.addClass("vjs-ruler");

          ruler_end.css("right", "0");
          ruler_end.css("margin", "3px"); 
          ruler_end.css("bottom", ($('.vjs-progress-control').css('height')));
          ruler_end.text(MillToTimecode(Zo+mD, 'PAL'));
          ruler_end.addClass("vjs-ruler");

          if(player.isFullscreen()) {

            ruler_begin.css('bottom', $('.vjs-progress-control').height()+30);
            ruler_middle.css('bottom', $('.vjs-progress-control').height()+30);
            ruler_end.css('bottom', $('.vjs-progress-control').height()+30);

          };

          $('.vjs-ruler').mouseover(function () {});

            for (var i=1; i<=Math.ceil(mD); i++) {

              if ((100*((i-1)+Math.ceil(Zo)-Zo)/mD)<=100) {

                if (((Zo!=0) && ((Math.floor((Math.floor(Zo)+i)/10))==(Math.ceil((Math.floor(Zo)+i)/10)))) || (Zo==0 && ((Math.floor((Math.floor(Zo)+i-1)/10))==(Math.ceil((Math.floor(Zo)+i-1)/10)))) ) {

                  var my_ruler = $("<div style='z-index:5; background-color: black; width: 1px; height: 0.7em;'></div>");

                } else { 

                  var my_ruler = $("<div style='z-index:5; background-color: black; width: 1px; height: 0.4em;'></div>");

                };

                $('.vjs-progress-control').append(my_ruler);  

                my_ruler.css("left", (100*((i-1)+Math.ceil(Zo)-Zo)/mD)+"%");
                my_ruler.css("bottom", "0");  
                my_ruler.css("position","absolute");
                my_ruler.addClass("vjs-ruler"); 

              };

            };

  }; 
  
	//function for auto_scrubbing - when you change time span on the ends of timeline in scaling mode
	function auto_scrubbing () {

    if (flag_as) {

      if (Tp>=0.99) { //when mouse position on timeline >= 99% of timeline's width (right end of timeline)

        if ((Zo+mD) <player.duration()) {
          setTimeout(function () {
               if ((Zo+mD)<player.duration()) {
                 Zo = Zo+mD/5;
                 player.currentTime(Zo+mD);
                 flag_as = true;
                 auto_scrubbing();
               }
          },1000);

        } else {

          Zo = player.duration() - mD;
          player.currentTime(player.duration());
          flag_as = false;

        }

        $('.vjs-ruler').remove();

        rulers();

    } else if (Tp<=0.01) { //when mouse position on timeline <= 1% of timeline's width (left end of timeline)

       if (Zo>(mD/5)) {
          setTimeout(function () {
              if (Zo>(mD/5)) {
                Zo = Zo-mD/5;
                player.currentTime(Zo);
                flag_as = true;

                auto_scrubbing();
              } 
          },1000);

       } else {

          Zo = 0;
          player.currentTime(Zo);
         flag_as = false;

       }

      $('.vjs-ruler').remove();

      rulers();

        } else {

       flag_as = false; 

       }

      };  

   };
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//this empty function is needed for solving conflict between mouse move action on timeline
	videojs.getComponent("SeekBar").prototype.handleMouseMove = function (e) { //erasing standart mousemove function


	};
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	player.on("mouseup",  function (e) {

      videojs.getComponent("SeekBar").prototype.getPercent = function () {

            var percent = player.currentTime() / player.duration();
            return percent >= 1 ? 1 : percent;

          };

          player.controlBar.progressControl.seekBar.update(); 

	});
  
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//end of scaling after mouse up event
	$(document).mouseup(function () {

    flag_sbs = false;

    playerShadow.el().style.opacity = '0';
    playerShadow.el().style.display = 'none';
    $('.vjs-ruler').remove();
    $('.vjs-marker').css('display', 'inline-block');
    $('.vjs-tc-marker').css('display', 'inline-block');
    $('.vjs-section').css('display', 'inline-block');

    flag_as = false; 

    videojs.getComponent("SeekBar").prototype.getPercent = function () { 

            var percent = player.currentTime() / player.duration();
            return percent >= 1 ? 1 : percent;

          };

          player.controlBar.progressControl.seekBar.update();

          flag_sbs = false;

          playerShadow.el().style.opacity = '0';

          $(".vjs-progress-control").css("font-size", "0.5em"); 


  });
  
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 
	//
    return this;
  };

  videojs.plugin('timescaling', timescaling);

})(window, window.videojs);

// function from timecode plugin
//Converts time in seconds to a broadcast timecode
//timeFormat: 'PAL', 'PALp', 'NTSC', 'STANDARD'
function MillToTimecode(seconds, TimeFormat) {

    //alert(milliseconds);

    var h = Math.floor(seconds / 3600);

    seconds = seconds - h * 3600;

    var m = Math.floor(seconds / 60);

    seconds = seconds - m * 60;

    var s = Math.floor(seconds);

    seconds = seconds - s;

    if (TimeFormat == 'PAL') {
        var f = Math.floor((seconds * 1000) / 40);
    }
    else if (TimeFormat == 'NTSC') {
        var f = Math.floor((seconds * 1000) / (100 / 3));
    }
    else if (TimeFormat == 'PALp') {
        var f = Math.floor((seconds * 1000) / 20);
    }
    else if (TimeFormat == 'STANDARD') {
        var f = Math.floor(seconds * 1000);
    }

    // Check if we need to show hours
    h = (h < 10) ? ("0" + h) + ":" : h + ":";

    // If hours are showing, we may need to add a leading zero.
    // Always show at least one digit of minutes.
    m = (((h) && m < 10) ? "0" + m : m) + ":";

    // Check if leading zero is need for seconds
    s = ((s < 10) ? "0" + s : s) + ":";

    f = (f < 10) ? "0" + f : f;

    if (TimeFormat == 'STANDARD')
        f = (f < 100) ? "0" + f : f;

    return h + m + s + f;
}
