/*
 * Video.js SuperMarkers
 * Markers with painting
 * Requires jQuery
 *
 * Copyright (c) 2016 Pavel Yanovsky
 */
 
 (function(window, videojs) {
  'use strict';

  window['videojs_timecode'] = { version: "0.0.1" };
  var supermarker = function() {
    var player = this;
 
    return this;
  };

  videojs.plugin('supermarker', supermarker);

})(window, window.videojs);


