/*
 * Video.js AddButtons
 * Add buttons
 * Requires jQuery
 *
 * Copyright (c) 2016 Pavel Yanovsky
 */
 
/* !!!example of html code for using this plugin!!!
<!--Add buttons varient for <video>-->
 
    <video id="my_video_1" crossorigin="anonymous" class="video-js vjs-default-skin" width="640px" height="267px" controls preload="none" poster='http://video-js.zencoder.com/oceans-clip.jpg' data-setup='{ "aspectRatio":"640:267", "playbackRates": [1, 1.5, 2],  "controlBar": { "volumeMenuButton": {"inline":false, "vertical":false}}, "addButtons":[{"name":"test_name_1", "content":"B1", "click":"test1"},{"name":"test_name_2", "content":"B2", "click":"test2"}]}'>

<!--example div for showing information from function test1 and test2-->
<div id='test_button'></div>	
<script type='text/javascript'>

//names of click function for new buttons the same as id of button (this name is taking from parameter "click" in html) 
  
function test1() { 
  
  $('#test_button').text("I'm button 1");
  
  };
  
function test2() {
  
  $('#test_button').text("I'm button 2");
  
  }  
  
</script>	
*/
 
 (function(window, videojs) {
  'use strict';

  window['videojs_addbuttons'] = { version: "0.0.1" };
  var addbuttons = function() {
    var player = this;
	
	if (player.options().addButtons) {

	var button_array = player.options().addButtons;
	 
	var ab = 0;
	
    //adding few buttons
	for (ab = 0; ab < button_array.length; ab++) { 

     var ClassButton = videojs.getComponent('Button');
      videojs.addButton = videojs.extend(ClassButton, {

          contructor: function(){

            ClassButton.call(this, player, options);
             this.on('click', this.onClick());

          },

      }); 

    
	var nb =  new videojs.addButton;
	
     var addButton = player.controlBar.addChild(nb, {});

     addButton.addClass("vjs-ab-"+button_array[ab].name);

    $('head').append("<style>.vjs-ab-"+button_array[ab].name+"::after { content:'"+button_array[ab].content+"'; }</style>");  //label of button
    $(".vjs-ab-"+button_array[ab].name).css({"cursor":"pointer", "position":"absolute", "right":""+(2.5+8.5+ab*2.5)+"em"});  
    $(".vjs-ab-"+button_array[ab].name).attr('id', button_array[ab].click); 
    $(".vjs-ab-"+button_array[ab].name).click(function() {eval(this.id+'()')});  

   }
	
	};

    return this;
  };

  videojs.plugin('addbuttons', addbuttons);

})(window, window.videojs);


